{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}

module Ciel.DCNEAT.GenomeSpec where

import Ciel.DCNEAT.Catalog.Internal
import Ciel.DCNEAT.Gene.Internal
import Ciel.DCNEAT.Genome.Internal
import Ciel.DCNEAT.Types
import qualified Data.Graph.Inductive.Graph as Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Set as Set
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random
import Test.Tasty
import Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC

-- | Helper function to check that two unsortable list have the same elements.
listEqual :: Eq a => [a] -> [a] -> Bool
listEqual a b = all (`elem` b) a && all (`elem` a) b

-- ============================================================================
--            Example Genomes useful for unit testing
-- ============================================================================

-- | Genome of type FFN with only two node genes and without
-- connection genes.
example1 :: Genome
example1 =
  Genome
    { genomeType = FFNGenome,
      nodes =
        Set.fromList [NodeGene (NodeId 1) Input, NodeGene (NodeId 2) Output],
      conns = Set.fromList []
    }

-- | Genome of type RNN with only two node genes and without
-- connection genes.
example2 :: Genome
example2 =
  Genome
    { genomeType = RNNGenome,
      nodes =
        Set.fromList [NodeGene (NodeId 1) Input, NodeGene (NodeId 2) Output],
      conns = Set.fromList []
    }

c13 = Connection (NodeId 1, NodeId 3)

c14 = Connection (NodeId 1, NodeId 4)

c23 = Connection (NodeId 2, NodeId 3)

c25 = Connection (NodeId 2, NodeId 5)

-- | Genome of type FFN with some connection genes
example3 :: Genome
example3 =
  Genome
    { genomeType = FFNGenome,
      nodes =
        Set.fromList
          [ NodeGene (NodeId 1) Input,
            NodeGene (NodeId 2) Input,
            NodeGene (NodeId 3) Output,
            NodeGene (NodeId 4) Output,
            NodeGene (NodeId 5) Hidden
          ],
      conns =
        Set.fromList
          [ ConnGene c13 (Weight 1) True (InnovNumber 1),
            ConnGene c14 (Weight (-1)) False (InnovNumber 2),
            ConnGene c23 (Weight 0.34) True (InnovNumber 3),
            ConnGene c25 (Weight 0.9) True (InnovNumber 4)
          ]
    }

-- | Genome of type RNN with some connection genes
example4 :: Genome
example4 =
  Genome
    { genomeType = RNNGenome,
      nodes =
        Set.fromList
          [ NodeGene (NodeId 1) Input,
            NodeGene (NodeId 2) Input,
            NodeGene (NodeId 3) Output,
            NodeGene (NodeId 4) Output,
            NodeGene (NodeId 5) Hidden
          ],
      conns =
        Set.fromList
          [ ConnGene c13 (Weight 1) True (InnovNumber 1),
            ConnGene c14 (Weight (-1)) False (InnovNumber 2),
            ConnGene c23 (Weight 0.34) True (InnovNumber 3),
            ConnGene c25 (Weight 0.9) True (InnovNumber 4)
          ]
    }

-- | Genome of type FFN without any possible new connections
example5 :: Genome
example5 =
  Genome
    { genomeType = FFNGenome,
      nodes =
        Set.fromList [NodeGene (NodeId 1) Input, NodeGene (NodeId 2) Output],
      conns =
        Set.fromList
          [ ConnGene
              (Connection (NodeId 1, NodeId 2))
              (Weight 1)
              True
              (InnovNumber 1)
          ]
    }

-- | Genome of type RNN without any possible new connections
example6 :: Genome
example6 =
  Genome
    { genomeType = RNNGenome,
      nodes =
        Set.fromList [NodeGene (NodeId 1) Input, NodeGene (NodeId 2) Output],
      conns =
        Set.fromList
          [ ConnGene
              (Connection (NodeId 1, NodeId 2))
              (Weight 1)
              True
              (InnovNumber 1),
            ConnGene
              (Connection (NodeId 1, NodeId 1))
              (Weight 1)
              True
              (InnovNumber 2),
            ConnGene
              (Connection (NodeId 2, NodeId 2))
              (Weight 1)
              True
              (InnovNumber 3),
            ConnGene
              (Connection (NodeId 2, NodeId 1))
              (Weight 1)
              True
              (InnovNumber 4)
          ]
    }

-- | Graph without a cycle
graphNoCycle :: Gr () ()
graphNoCycle = Graph.mkUGraph [1, 2] [(1, 2)]

-- | Graph with cycle
graphCycle :: Gr () ()
graphCycle = Graph.mkUGraph [1, 2] [(1, 2), (2, 1)]

-- ============================================================================
--                            Unit Tests
-- ============================================================================
unitTestsFNNunConnNodes =
  testGroup
    "Unit tests for FFN Genome and the unConnNodes function"
    [ testCase "when the genome has an empty connection genes list" $
        unConnNodes example1 @?= [Connection (NodeId 1, NodeId 2)],
      testCase "when the genome has some connection genes" $
        let expectedOutput =
              [ Connection (NodeId 1, NodeId 5),
                Connection (NodeId 2, NodeId 4),
                Connection (NodeId 5, NodeId 3),
                Connection (NodeId 5, NodeId 4)
              ]
            obtainedOutput = unConnNodes example3
         in listEqual expectedOutput obtainedOutput @?= True,
      testCase "when the genome has no more possible connections" $
        unConnNodes example5 @?= []
    ]

unitTestsRNNunConnNodes =
  testGroup
    "Unit tests for RNN Genome and the unConnNodes function"
    [ testCase "when the genome has an empty connection genes list" $
        let expectedOutput =
              [ Connection (NodeId 1, NodeId 2),
                Connection (NodeId 1, NodeId 1),
                Connection (NodeId 2, NodeId 2),
                Connection (NodeId 2, NodeId 1)
              ]
            obtainedOutput = unConnNodes example2
         in listEqual expectedOutput obtainedOutput @?= True,
      testCase "when the genome has some connection genes" $
        let expectedOutput =
              [ Connection (NodeId 1, NodeId 1),
                Connection (NodeId 1, NodeId 2),
                Connection (NodeId 1, NodeId 5),
                Connection (NodeId 2, NodeId 2),
                Connection (NodeId 2, NodeId 1),
                Connection (NodeId 2, NodeId 4),
                Connection (NodeId 3, NodeId 3),
                Connection (NodeId 3, NodeId 1),
                Connection (NodeId 3, NodeId 2),
                Connection (NodeId 3, NodeId 4),
                Connection (NodeId 3, NodeId 5),
                Connection (NodeId 4, NodeId 1),
                Connection (NodeId 4, NodeId 2),
                Connection (NodeId 4, NodeId 3),
                Connection (NodeId 4, NodeId 4),
                Connection (NodeId 4, NodeId 5),
                Connection (NodeId 5, NodeId 1),
                Connection (NodeId 5, NodeId 2),
                Connection (NodeId 5, NodeId 3),
                Connection (NodeId 5, NodeId 4),
                Connection (NodeId 5, NodeId 5)
              ]
            obtainedOutput = unConnNodes example4
         in listEqual expectedOutput obtainedOutput @?= True,
      testCase "when the genome has no more possible connections" $
        unConnNodes example6 @?= []
    ]

unitTestsFFNAllConns =
  testGroup
    "Unit tests for FFN Genome and the allConns function"
    [ testCase "only input and output nodes" $
        let expectedOutput = [Connection (NodeId 1, NodeId 2)]
            obtainedOutput = allConns example1
         in listEqual expectedOutput obtainedOutput @?= True
    ]

unitTestsRNNAllConns =
  testGroup
    "Unit tests for RNN Genome and the allConns function"
    [ testCase "only input and output nodes" $
        let expectedOutput =
              [ Connection (NodeId 1, NodeId 1),
                Connection (NodeId 1, NodeId 2),
                Connection (NodeId 2, NodeId 2),
                Connection (NodeId 2, NodeId 1)
              ]
            obtainedOutput = allConns example2
         in listEqual expectedOutput obtainedOutput @?= True
    ]

unitTestsHasCycle =
  testGroup
    "Unit tests for hasCycle function"
    [ testCase "Graph without cycles" $ hasCycle graphNoCycle @?= False,
      testCase "Graph with cycle" $ hasCycle graphCycle @?= True
    ]

unitTestsFFN =
  testGroup "FFN Genome" [unitTestsFNNunConnNodes, unitTestsFFNAllConns]

unitTestsRNN =
  testGroup "RNN Genome" [unitTestsRNNunConnNodes, unitTestsRNNAllConns]

-- | A new genome should only have as connection its input nodes connected to
-- its output nodes
propMkGenome :: IO Bool
propMkGenome = do
  (g, nI, nO) <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI <- randomR (1, 10)
    nO <- randomR (1, 10)
    g <- mkGenome nI nO FFNGenome
    return (g, nI, nO)
  return $ (==) (Set.size $ conns g) (fromIntegral (nI * nO))

-- | Two new genomes with the same number of inputs and outputs
-- should be equal
propMkGenomeEq :: IO Bool
propMkGenomeEq = do
  (g1, g2) <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI <- randomR (1, 10)
    nO <- randomR (1, 10)
    g1 <- mkGenome nI nO FFNGenome
    g2 <- mkGenome nI nO FFNGenome
    return (g1, g2)
  return (g1 == g2)

-- | The distance between two genomes should be positive
propDistanceMaxPos :: IO Bool
propDistanceMaxPos = do
  (g1, g2) <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI1 <- randomR (1, 10)
    nO1 <- randomR (1, 10)
    nI2 <- randomR (1, 10)
    nO2 <- randomR (1, 10)
    g1 <- mkGenome nI1 nO1 FFNGenome
    g2 <- mkGenome nI2 nO2 FFNGenome
    return (g1, g2)
  return (distanceMax g1 g2 >= 0)

-- | The distance between two genomes should be symmetry
propDistanceMaxSym :: IO Bool
propDistanceMaxSym = do
  (g1, g2) <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI1 <- randomR (1, 10)
    nO1 <- randomR (1, 10)
    nI2 <- randomR (1, 10)
    nO2 <- randomR (1, 10)
    g1 <- mkGenome nI1 nO1 FFNGenome
    g2 <- mkGenome nI2 nO2 FFNGenome
    return (g1, g2)
  return (distanceMax g1 g2 == distanceMax g2 g1)

-- | The distance between a genome to itself is equal to 0
propDistanceMaxZero :: IO Bool
propDistanceMaxZero = do
  g1 <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI <- randomR (1, 10)
    nO <- randomR (1, 10)
    mkGenome nI nO FFNGenome
  return (distanceMax g1 g1 == 0)

-- | The distance between two genomes should follow the
-- triangular inequality
propDistanceMaxTriangular :: IO Bool
propDistanceMaxTriangular = do
  (g1, g2, g3) <- fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    nI1 <- randomR (1, 10)
    nO1 <- randomR (1, 10)
    nI2 <- randomR (1, 10)
    nO2 <- randomR (1, 10)
    nI3 <- randomR (1, 10)
    nO3 <- randomR (1, 10)
    g1 <- mkGenome nI1 nO1 FFNGenome
    g2 <- mkGenome nI2 nO2 FFNGenome
    g3 <- mkGenome nI3 nO3 FFNGenome
    return (g1, g2, g3)
  return (distanceMax g1 g2 <= distanceMax g1 g3 + distanceMax g2 g3)

test_unitTestsGenome =
  testGroup
    "Unit Test for Genome"
    [unitTestsRNN, unitTestsFFN, unitTestsHasCycle]

test_propTestsGenome =
  testGroup
    "Property Tests for Genome"
    [ QC.testProperty
        "A new genome should only have as connections its input nodes connected to its output nodes"
        $ QC.again
        $ QC.ioProperty propMkGenome,
      QC.testProperty
        "Two genome produced with the same inputs and outputs should be equal"
        $ QC.again
        $ QC.ioProperty propMkGenomeEq,
      QC.testProperty
        "The distance between genomes (max norm) should follow the identity of indiscernibles condition"
        $ QC.again
        $ QC.ioProperty propDistanceMaxZero,
      QC.testProperty
        "The distance between genomes (max norm) should follow the triangular inequality"
        $ QC.again
        $ QC.ioProperty propDistanceMaxTriangular,
      QC.testProperty "The distance between genomes (max norm) should be positive"
        $ QC.again
        $ QC.ioProperty propDistanceMaxPos,
      QC.testProperty "The distance between genomes (max norm) should be symmetric"
        $ QC.again
        $ QC.ioProperty propDistanceMaxSym
    ]
