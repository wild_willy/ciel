module Ciel.DCNEAT.CatalogSpec where

import Ciel.DCNEAT.Catalog.Internal
import Ciel.DCNEAT.Types
import Data.Map
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random
import System.Random (mkStdGen)
import Test.Tasty
import Test.Tasty.HUnit
import qualified Test.Tasty.QuickCheck as QC

-- | Property to describe adding a connection to
-- an initial state (empty catalog).
propAddConnEmpty :: IO Bool
propAddConnEmpty = do
  a <- runM $ runRandomIO random
  b <- runM $ runRandomIO random
  let c = Connection (a, b)
  (_, inn) <- runM $ atomicStateToIO mkInitialState $ addConnection c
  return (inn == InnovNumber 1)

-- | Property to describe that adding the same connection twice
-- to the catalog returns the same innovation number.
propAddConnTwice :: IO Bool
propAddConnTwice = do
  a <- runM $ runRandomIO random
  b <- runM $ runRandomIO random
  let c = Connection (a, b)
  fmap snd $ runM $ atomicStateToIO mkInitialState $ do
    inn1 <- addConnection c
    inn2 <- addConnection c
    return (inn1 == inn2)

-- | Property tests for Adding a connection to the catalog
test_propsAddConnection =
  testGroup
    "Property tests for adding a connection to the catalog"
    [ QC.testProperty "Adding a connection to an empty catalog" $ QC.again $ QC.ioProperty propAddConnEmpty,
      QC.testProperty "Adding a connection twice to an empty catalog" $ QC.again $ QC.ioProperty propAddConnTwice
    ]
