{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}

module Ciel.DCNEAT.GeneSpec where

import Ciel.DCNEAT.Catalog.Internal
import Ciel.DCNEAT.Gene.Internal
import Ciel.DCNEAT.Types
import Data.List
import Data.Ord
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random
import System.Random (mkStdGen)
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck as QC

-- | Arbitrary Instance of a innovation Number
deriving instance Arbitrary InnovationNumber

-- | Arbitrary Instance of a node id
deriving instance Arbitrary NodeId

-- | Arbitrary Instance of a Connection
deriving instance Arbitrary Connection

-- | Helper function to create Connection Genes from
-- innovation numbers
createConnGene :: InnovationNumber -> ConnectionGene
createConnGene innovN =
  ConnGene
    { conn = Connection (NodeId 1, NodeId 2),
      weight = Weight 0.1,
      enabled = True,
      innov = innovN
    }

-- | Connection genes are sorted by their innovation numbers
propConnGenesSort :: [InnovationNumber] -> Bool
propConnGenesSort innovNs = sort innovNs == map innov (sort $ map createConnGene innovNs)

-- | Connection genes are equal if their innovation genes are equal
propConnGenesEq :: InnovationNumber -> InnovationNumber -> Bool
propConnGenesEq in1 in2 = (in1 == in2) == (createConnGene in1 == createConnGene in2)

-- | All connection genes are created enabled
propConnGenesEnabled :: Connection -> InnovationNumber -> Bool
propConnGenesEnabled c inn =
  let (_, cg) = run $ runRandom (mkStdGen 1) $ mkConnectionGene c inn
   in enabled cg

-- | All random weights created are between [-1, 1]
propMkRandomWeight :: IO Bool
propMkRandomWeight = do
  w <- runM $ runRandomIO mkRandomWeight
  return $ (w <= 1) && (w >= (-1))

-- | Creating connection genes with the same connection
-- results on equal connection genes.
propAddConnGene :: IO Bool
propAddConnGene = do
  a <- runM $ runRandomIO random
  b <- runM $ runRandomIO random
  let c = Connection (a, b)
  fmap snd $ runM $ runRandomIO $ atomicStateToIO mkInitialState $ do
    cg1 <- addConnGene c
    cg2 <- addConnGene c
    return (cg1 == cg2)

-- | Properties for connection genes
test_propsConnGenes =
  testGroup
    "Property tests for Connection Genes"
    [ QC.testProperty
        "Connection genes are sorted by their innovation numbers"
        propConnGenesSort,
      QC.testProperty
        "Connection genes are equal if their innovation numbers are equal"
        propConnGenesEq,
      QC.testProperty
        "All connections created are enabled"
        propConnGenesEnabled,
      QC.testProperty
        "All weights are between -1 and 1"
        $ QC.again
        $ QC.ioProperty propMkRandomWeight,
      QC.testProperty
        "Adding connection genes to the population with the same structure result in equal connection genes"
        $ QC.again
        $ QC.ioProperty propAddConnGene
    ]
