# Ciel
Ciel is first an implementation of Deterministically Crowded Neuro Evolution of Augmenting Topologies (DC-NEAT).

# Features
- [] DC-NEAT Implementation
- [] Winery support store populations for later review
- [] Parallel computation 
