{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Ciel.DCNEAT.Types where

import Data.Map (Map)
import Data.Word (Word64)
import System.Random (Random)

-- | Type for identifying nodes in DCNEAT
newtype NodeId
  = NodeId
      { unNodeId :: Word64
      }
  deriving (Show, Eq, Ord, Random)

-- | Type for describing a connection (from, to)
newtype Connection
  = Connection
      { unConnection :: (NodeId, NodeId)
      }
  deriving (Show, Eq, Ord)

-- | Type for describing the weight of a connection
newtype Weight
  = Weight
      { unWeight :: Double
      }
  deriving (Show, Eq, Ord, Num, Random)

-- | Type for describing the innovation number of connection
newtype InnovationNumber
  = InnovNumber
      { unInnovNumber :: Integer
      }
  deriving (Show, Eq, Ord, Num)

-- | Type for describing the innovation number and connection
-- catalog of a population. This catalog associates a connection with an
-- innovation number. All connections have the same innovation number through
-- the generations. Thus, this catalog must be unique for a population and
-- a be kept through the generations.
newtype InnovationCatalog
  = InnovCatalog {unInnovCatalog :: Map Connection InnovationNumber}
  deriving (Show, Eq)
