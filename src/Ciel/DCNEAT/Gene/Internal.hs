{-# LANGUAGE FlexibleContexts #-}

module Ciel.DCNEAT.Gene.Internal where

import Ciel.DCNEAT.Catalog.Internal
import Ciel.DCNEAT.Types
import Data.Function (on)
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random

-- |
--  Type for classifying the type of nodes
data NodeType
  = -- | A node that receives information from the environment
    Input
  | -- | A node inside the network
    Hidden
  | -- | A node that outputs informatino to the environment
    Output
  deriving (Show, Eq)

-- |
--  A gene to describe a node
data NodeGene
  = NodeGene
      { -- | The node's indentification
        node :: !NodeId,
        -- | The node's type
        nodeType :: !NodeType
      }
  deriving (Show, Eq)

-- | Node genes can be ordered by their node ids
instance Ord NodeGene where
  compare = compare `on` node

-- |
--  A gene to describe a connection
data ConnectionGene
  = ConnGene
      { -- | The connection described by the gene
        conn :: !Connection,
        -- | The weight of the connection [-1, 1]
        weight :: !Weight,
        -- | Whether or not the connection is enabled
        enabled :: !Bool,
        -- | The innovation number of the gene
        innov :: !InnovationNumber
      }
  deriving (Show)

-- | Two connection genes are equal if their innovation numbers are the same
instance Eq ConnectionGene where
  (==) = (==) `on` innov

-- | Connection genes can be ordered by their innovation numbers
instance Ord ConnectionGene where
  compare = compare `on` innov

-- | Creates a Connection gene from the given Connection and Innovation Number with
-- a random weight. The produced connection gene is enabled.
mkConnectionGene ::
  Member Random r =>
  Connection ->
  InnovationNumber ->
  Sem r ConnectionGene
mkConnectionGene c inn = do
  w <- mkRandomWeight
  return $ ConnGene
    { conn = c,
      weight = w,
      enabled = True,
      innov = inn
    }

-- | Calculates a random weight
mkRandomWeight :: Member Random r => Sem r Weight
mkRandomWeight = randomR (Weight (-1), Weight 1)

-- | Add a connection gene to the population.
addConnGene ::
  Members [Random, AtomicState DCNEATState] r =>
  Connection ->
  Sem r ConnectionGene
addConnGene c = do
  inn <- addConnection c
  mkConnectionGene c inn
