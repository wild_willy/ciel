module Ciel.DCNEAT.Utils where

-- | This function applies the same function to all components of
-- tuple.
onTuple :: (a, a) -> (a -> b) -> (b, b)
onTuple (a, b) f = (f a, f b)
