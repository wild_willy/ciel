{-# LANGUAGE RecordWildCards #-}

module Ciel.DCNEAT.Genome.Internal where

import Ciel.DCNEAT.Catalog.Internal
import Ciel.DCNEAT.Gene.Internal
import Ciel.DCNEAT.Types
import Ciel.DCNEAT.Utils
import Data.Function (on)
import Data.Graph.Inductive.Graph (Graph, Node, suc)
import qualified Data.Graph.Inductive.Graph as Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Graph.Inductive.Query.DFS
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Word (Word64)
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random

-- | Type for determining the type of network.
-- This affects the mutation and evaluation of the genome
data GenomeType
  = -- | Feed Forward Network: does not allows cycles
    FFNGenome
  | -- | Recurrent Neural Network: allows cycles
    RNNGenome
  deriving (Show, Eq)

-- | A genome contains the genes necessary to build an artifial neural network
data Genome
  = Genome
      { -- | The type of the network represented by the genome
        genomeType :: !GenomeType,
        -- | The node genes
        nodes :: !(Set NodeGene),
        -- | The connection genes
        conns :: !(Set ConnectionGene)
      }
  deriving (Show, Eq)

-- | Creates the initial configuration of a genome,
-- a network with all input nodes connected to the outputs.
-- Each connection will have random weight.
-- The produced connections are added to the catalog.
mkGenome ::
  Members [AtomicState DCNEATState, Random] r =>
  Word64 ->
  Word64 ->
  GenomeType ->
  Sem r Genome
mkGenome nInputNodes nOutputNodes gType =
  let inputNodes = [NodeId x | x <- [1 .. nInputNodes]]
      outputNodes = [NodeId x | x <- [(nInputNodes + 1) .. (nInputNodes + nOutputNodes)]]
      cs = [Connection (x, y) | x <- inputNodes, y <- outputNodes]
      inputNgs = map (`NodeGene` Input) inputNodes
      outputNgs = map (`NodeGene` Output) outputNodes
      nodeGenes = inputNgs ++ outputNgs
   in do
        connGenes <- mapM addConnGene cs
        return $ Genome
          { nodes = Set.fromList nodeGenes,
            conns = Set.fromList connGenes,
            genomeType = gType
          }

-- | Get the possible new connections from a genome it takes into account
-- the type of genome. Therefore, if the genome is of type FFN the new connections
-- cannot form cycles when added to the genome. For type RNN everything is valid.
unConnNodes :: Genome -> [Connection]
unConnNodes g@Genome {..} =
  let currentConns = map conn $ Set.toList conns
      possibleConns = filter (`notElem` currentConns) $ allConns g
   in case genomeType of
        RNNGenome -> possibleConns
        FFNGenome -> filter (not . formsCycle gr) possibleConns
          where
            gr = transformToGraph g

-- | From the nodes of a Genome get all possible connections
-- For genomes of type RNN this list of possible connections includes
-- self loops and other kind of recurrent connections.
-- For genomes of type FFN this list includes only feedforward and possible feedforward
-- connections (Input -> Output) and (Hidden -> Hidden).
allConns :: Genome -> [Connection]
allConns Genome {..} =
  let cs = Set.toList $ Set.cartesianProduct nodes nodes
      ho (a, b) = Connection (node a, node b)
      go (a, b) =
        case (nodeType a, nodeType b) of
          (Output, Output) -> False
          (Output, Input) -> False
          (Output, Hidden) -> False
          (Hidden, Input) -> False
          (Input, Input) -> False
          _ -> True
   in case genomeType of
        FFNGenome -> map ho $ filter go cs
        RNNGenome -> map ho cs

-- | The distance between two genomes, defined as the maximum delta of the
-- match between connection genes from the two genomes
distanceMax :: Genome -> Genome -> Double
distanceMax g1 g2 =
  maximum
    $ map (\(Weight a, Weight b) -> abs (a - b))
    $ matchConnGenes (Set.toAscList $ conns g1) (Set.toAscList $ conns g2)

-- ================================================================================
--                           Helper Functions
-- ================================================================================

-- | Helper function to determine whether or not a graph contains a cycle.
-- An empty graph has no cycles.
hasCycle :: Graph gr => gr a b -> Bool
hasCycle gr
  | Graph.isEmpty gr = False
  | not $ hasLeaf gr = True
  | otherwise = hasCycle $ delLeaf gr

-- | Helper function to determine whether or not the given node is a leaf
isLeaf :: Graph gr => gr a b -> Node -> Bool
isLeaf gr n = null $ suc gr n

-- | Helper function to determine whether or not the given graph has a node
hasLeaf :: Graph gr => gr a b -> Bool
hasLeaf gr = any (isLeaf gr) $ Graph.nodes gr

-- | Helper function to delete the first found leaf in a graph
delLeaf :: Graph gr => gr a b -> gr a b
delLeaf gr =
  let leaf = head [x | x <- Graph.nodes gr, isLeaf gr x]
   in snd $ Graph.match leaf gr

-- | Helper function to transform a genome to graph
transformToGraph :: Genome -> UGr
transformToGraph Genome {..} =
  let ns = map (fromIntegral . unNodeId . node) $ Set.toList nodes
      cs = map ((`onTuple` (fromIntegral . unNodeId)) . unConnection . conn) $ Set.toList conns
   in Graph.mkUGraph ns cs

-- | Helper function to insert a new connection to Graph
insertConnection :: UGr -> Connection -> UGr
insertConnection gr (Connection (NodeId a, NodeId b)) =
  Graph.insEdge (fromIntegral a, fromIntegral b, ()) gr

-- | Helper function to determine whether or not introducing the given connection
-- to the graph creates a cycle.
formsCycle :: UGr -> Connection -> Bool
formsCycle gr c = hasCycle $ insertConnection gr c

-- | Helper to match the connection genes from two genomes
matchConnGenes :: [ConnectionGene] -> [ConnectionGene] -> [(Weight, Weight)]
matchConnGenes [] [] = []
matchConnGenes [x] [] = [(weight x, Weight 0)]
matchConnGenes [] [x] = [(Weight 0, weight x)]
matchConnGenes (x : xs) [] = (weight x, Weight 0) : matchConnGenes xs []
matchConnGenes [] (y : ys) = (Weight 0, weight y) : matchConnGenes [] ys
matchConnGenes (x : xs) (y : ys)
  | x == y = (weight x, weight y) : matchConnGenes xs ys
  | x < y = (weight x, Weight 0) : matchConnGenes xs (y : ys)
  | x > y = (Weight 0, weight y) : matchConnGenes (x : xs) ys
