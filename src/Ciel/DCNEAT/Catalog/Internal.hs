module Ciel.DCNEAT.Catalog.Internal where

import Ciel.DCNEAT.Types
import qualified Data.Map as Map
import Polysemy
import Polysemy.AtomicState
import Polysemy.Random

-- | Data that tracks the state of a run in DCNEAT
data DCNEATState
  = DCNEATState
      { -- | The Innovation Number catalog
        catalog :: !InnovationCatalog,
        -- | The next available Innovation Number
        nextInnovNumber :: !InnovationNumber
      }
  deriving (Show, Eq)

-- | Initial State start with empty catalog and the next available
-- innovation number is 1
mkInitialState :: DCNEATState
mkInitialState = DCNEATState (InnovCatalog Map.empty) (InnovNumber 1)

-- | Function for adding a connection to the catalog.
-- If the connection is already present in the catalog,
-- return its respective innovation number. Otherwise,
-- add the connection to the catalog and return the corresponding
-- innovation number whilst updating the tracked innovation number
addConnection :: Member (AtomicState DCNEATState) r => Connection -> Sem r InnovationNumber
addConnection c = do
  DCNEATState cat inn <- atomicGet
  case Map.lookup c (unInnovCatalog cat) of
    Nothing -> do
      let curInn = inn
          newCat = InnovCatalog $ Map.insert c curInn (unInnovCatalog cat)
          newInn = curInn + 1
      atomicPut $ DCNEATState newCat newInn
      return curInn
    Just x -> return x
