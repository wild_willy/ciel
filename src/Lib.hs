module Lib
  ( someFunc,
  )
where

import Data.Bool

f :: Int -> Int
f x = 2 * x

someFunc = print $ f 7
